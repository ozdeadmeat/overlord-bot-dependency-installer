# Overlord Bot Dependency Installer
Script is designed to speed up the installation process for Overlord Bot.

Applications Automatically installed are:

- Microsoft Visual C++ (x64)   - 14.29.30037
- Microsoft .NET Runtime (x64) - 5.0.7
- Java Development Kit (x64)   - 11.0.2
- JRuby (x64)                  - 9.2.19.0
- PostgreSQL (x64)             - 13.3.2
- PostGIS                      - Manual Install using Stack Builder

User Options for PostgreSQL installation are available by editing the script (line 19 - 23)

Tested with Windows 10 Pro using Powershell 5.0
